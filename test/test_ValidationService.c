#include "unity.h"
#include "support/unix_logs.h"
#include "util/types.h"
#include "util/logs.h"
#include "service/validate/ValidationService.h"
#include "mock_functions.h"

ValidationService service;

void setUp()
{
	service = ValidationService_create(function1_byte_bool);
}
void tearDown() {}

void test_return_last_valid_mode()
{
  function1_byte_bool_ExpectAndReturn(7, true);
  TEST_ASSERT_EQUAL(7, ValidationService_processValue(&service, 7));

  function1_byte_bool_ExpectAndReturn(10, true);
  TEST_ASSERT_EQUAL(10, ValidationService_processValue(&service, 10));

  function1_byte_bool_ExpectAndReturn(3, false);
  TEST_ASSERT_EQUAL(10, ValidationService_processValue(&service, 3));

  function1_byte_bool_ExpectAndReturn(5, false);
  TEST_ASSERT_EQUAL(10, ValidationService_processValue(&service, 5));

  function1_byte_bool_ExpectAndReturn(12, true);
  TEST_ASSERT_EQUAL(12, ValidationService_processValue(&service, 12));
}
