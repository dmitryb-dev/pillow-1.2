#include "unity.h"
#include "platform/IO.h"
#include "support/unix_logs.h"
#include "util/logs.h"
#include "service/validate/ValueValidator.h"
#include "service/validate/StateValidator.h"
#include "service/PanelService.h"
#include "util/utils.h"
#include "mock_ValidationService.h"
#include "mock_IO.h"

ValidationService validator_mock;

void setUp()
{
  ValidationService_create_IgnoreAndReturn(validator_mock);
  ValidationService_createWithInitVal_IgnoreAndReturn(validator_mock);
  PanelService_init();
}
void tearDown() {}

void test_updating_mode()
{
  IO_getValue_ExpectAndReturn(WORKING_MODE_TOGGLE, 1);
  ValidationService_processValue_IgnoreAndReturn(2);

  PanelService_updateMode();
  TEST_ASSERT_EQUAL(MODE_MANUAL, PanelService_getMode());
}

void test_updating_volume()
{
  IO_getValue_ExpectAndReturn(VOLUME_SLIDER, 1);
  ValidationService_processValue_IgnoreAndReturn(2);

  TEST_ASSERT_EQUAL(0.02f, PanelService_getVolume());
}

void test_updating_duration()
{
  IO_getValue_ExpectAndReturn(DURATION_SLIDER, 1);
  ValidationService_processValue_IgnoreAndReturn(2);

  TEST_ASSERT_EQUAL(0.02f, PanelService_getDuration());
}
