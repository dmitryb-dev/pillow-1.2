#include "unity.h"
#include "platform/IO.h"
#include "util/channel.h"
#include "task/AudioTask.h"
#include "task/BatteryTask.h"
#include "task/HeadSensorTask.h"
#include "task/PanelTask.h"
#include "task/TimerTask.h"
#include "lifecycle/Pillow.h"
#include <sys/time.h>
#include "support/unix_logs.h"
#include "service/UpdateSchedulerService.h"
#include "service/BatteryService.h"
#include "service/PanelService.h"
#include "service/HeadSensorService.h"
#include "service/audio/AudioService.h"
#include "service/audio/AudioPlayer.h"
#include "service/timer/IntervalTimer.h"
#include "util/logs.h"
#include "lifecycle/lifecycle.h"
#include "lifecycle/SystemState.h"
#include "service/validate/ValueValidator.h"
#include "service/validate/StateValidator.h"
#include "service/validate/ValidationService.h"
#include "util/utils.h"
#include "mock_IO.h"
#include "mock_interrupts.h"

long long current_time_nanos()
{
  struct timeval tv;
  gettimeofday(&tv, 0);
  return tv.tv_sec * 1000000LL + tv.tv_usec;
}

long long start_time;
void setUp()
{
  AudioTask_init();
  BatteryTask_init();
  HeadSensorTask_init();
  PanelTask_init();
  HeadSensorService_init(30, 1);

  IO_getValue_IgnoreAndReturn(0);
  IO_readFile_IgnoreAndReturn(1);
  static char sensor_data[30];
  IO_getHeadSensorData_IgnoreAndReturn(&sensor_data[0]);

  start_time = current_time_nanos();
}

void tearDown()
{
  long long end_time = current_time_nanos();
  printf("\n ------ \nTime, nanos: %lld", end_time - start_time);
}

void _test_all_tasks_per_cycle()
{
  for (int i = 0; i < 100000; i++)
  {
    AudioTask_updateState();
    BatteryTask_updateState(-1);
    HeadSensorTask_updateState();
    PanelTask_updateState();
    TimerTask_updateState();
  }
}
