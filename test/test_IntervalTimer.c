#include "unity.h"
#include "service/timer/IntervalTimer.h"
#include "mock_interrupts.h"
#include "util/logs.h"
#include "support/unix_logs.h"

void (*intercepted_on_tick_callback)(void);
void callback_interceptor(
  Timer timer, int interval, void (*callback)(void), int num_calls)
{
  TEST_ASSERT_EQUAL(INTERVAL_TIMER, timer);
  intercepted_on_tick_callback = callback;
}

void setUp()
{
  interrupts_registerTimer_StubWithCallback(callback_interceptor);
  IntervalTimer_start(10);
}
void tearDown()
{
  interrupts_unregisterTimer_Expect(INTERVAL_TIMER);
  IntervalTimer_stop();
}

void test_ring()
{
  TEST_ASSERT_FALSE(IntervalTimer_checkIsRingedAndReset());
  intercepted_on_tick_callback();
  TEST_ASSERT_TRUE(IntervalTimer_checkIsRingedAndReset());
  TEST_ASSERT_FALSE(IntervalTimer_checkIsRingedAndReset());
}
