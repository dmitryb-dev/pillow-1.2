#include <stdio.h>
#include "platform/logs.h"
#include "util/logs.h"

void platform_log(const char* msg)
{
	printf("%s", msg);
}
