#include "unity.h"
#include "util/channel.h"

ByteChannel channel;

void setUp()
{
  static byte buffer[3];
  channel = ByteChannel_create(&buffer[0], 3);
}
void tearDown() {}

void test_write_until_end()
{
  TEST_ASSERT_FALSE(ByteChannel_hasNew(&channel));

  for (int i = 0; i < 3; i++)
  {
    TEST_ASSERT_TRUE(ByteChannel_hasSpace(&channel));
    ByteChannel_push(&channel, i);
    TEST_ASSERT_TRUE(ByteChannel_hasNew(&channel));
  }

  TEST_ASSERT_FALSE(ByteChannel_hasSpace(&channel));
}

void test_write_index_goes_to_start()
{
  test_write_until_end();

  TEST_ASSERT_TRUE(ByteChannel_hasNew(&channel));
  TEST_ASSERT_EQUAL(0, ByteChannel_pop(&channel));

  TEST_ASSERT_TRUE(ByteChannel_hasNew(&channel));
  TEST_ASSERT_TRUE(ByteChannel_hasSpace(&channel));

  ByteChannel_push(&channel, 3);
  TEST_ASSERT_FALSE(ByteChannel_hasSpace(&channel));
}

void test_write_until_read_index_reached()
{
  test_write_index_goes_to_start();

  TEST_ASSERT_FALSE(ByteChannel_hasSpace(&channel));

  TEST_ASSERT_TRUE(ByteChannel_hasNew(&channel));
  TEST_ASSERT_EQUAL(1, ByteChannel_pop(&channel));

  TEST_ASSERT_TRUE(ByteChannel_hasSpace(&channel));
  ByteChannel_push(&channel, 4);
  TEST_ASSERT_FALSE(ByteChannel_hasSpace(&channel));
}

void test_read_index_goes_to_start()
{
  test_write_until_read_index_reached();

  for (int i = 2; i < 4; i++)
  {
    TEST_ASSERT_TRUE(ByteChannel_hasNew(&channel));
    TEST_ASSERT_EQUAL(i, ByteChannel_pop(&channel));
  }

  TEST_ASSERT_TRUE(ByteChannel_hasNew(&channel));
}

void test_read_until_write_index_reached()
{
  test_read_index_goes_to_start();

  TEST_ASSERT_TRUE(ByteChannel_hasNew(&channel));
  TEST_ASSERT_EQUAL(4, ByteChannel_pop(&channel));
  TEST_ASSERT_FALSE(ByteChannel_hasNew(&channel));
}
