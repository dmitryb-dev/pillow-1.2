#include "unity.h"
#include "service/HeadSensorService.h"
#include "util/types.h"
#include "mock_ValidationService.h"
#include "mock_IO.h"
#include "util/logs.h"
#include "service/validate/ValueValidator.h"
#include "support/unix_logs.h"
#include "util/utils.h"

ValidationService validator_mock;
byte process_value_mock(ValidationService *service, byte value, int num_calls)
{
  return value;
}

void setUp()
{
  ValidationService_create_IgnoreAndReturn(validator_mock);
  ValidationService_processValue_StubWithCallback(process_value_mock);
  HeadSensorService_init(5, 3);

  HeadSensorService_enable();
}
void tearDown() {}

void test_head_out()
{
  byte data1[] = { 1, 1, 1, 1, 1 };
  IO_getHeadSensorData_ExpectAndReturn(&data1[0]);
  TEST_ASSERT_EQUAL(HEAD_OUT, HeadSensorService_getState());

  byte data2[] = { 2, 1, 2, 1, 2 };
  IO_getHeadSensorData_ExpectAndReturn(&data2[0]);
  TEST_ASSERT_EQUAL(HEAD_OUT, HeadSensorService_getState());

  byte data3[] = { 1, 2, 5, 2, 1 };
  IO_getHeadSensorData_ExpectAndReturn(&data3[0]);
  TEST_ASSERT_EQUAL(HEAD_OUT, HeadSensorService_getState());

  byte data4[] = { 3, 3, 2, 3, 3 };
  IO_getHeadSensorData_ExpectAndReturn(&data4[0]);
  TEST_ASSERT_EQUAL(HEAD_OUT, HeadSensorService_getState());
}

void test_head_in()
{
  byte data1[] = { 3, 3, 3, 3, 3 };
  IO_getHeadSensorData_ExpectAndReturn(&data1[0]);
  TEST_ASSERT_EQUAL(HEAD_IN, HeadSensorService_getState());

  byte data2[] = { 1, 1, 9, 1, 1 };
  IO_getHeadSensorData_ExpectAndReturn(&data2[0]);
  TEST_ASSERT_EQUAL(HEAD_IN, HeadSensorService_getState());

  byte data3[] = { 2, 3, 5, 3, 2 };
  IO_getHeadSensorData_ExpectAndReturn(&data3[0]);
  TEST_ASSERT_EQUAL(HEAD_IN, HeadSensorService_getState());
}

void test_disabled()
{
  HeadSensorService_disable();
  TEST_ASSERT_EQUAL(SENSOR_DISABLED, HeadSensorService_getState());
}
