#include "unity.h"
#include "service/validate/ValueValidator.h"
#include "service/validate/StateValidator.h"
#include "util/utils.h"

StateValidator validator;

void setUp() {
	validator = StateValidator_createWithInitVal(2, -1);
}
void tearDown() {}

void test_non_valid_when_not_enough_values()
{
	TEST_ASSERT_EQUAL(false, StateValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(false, StateValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(true, StateValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(true, StateValidator_isValid(&validator, 1));
}

void test_reset_when_value_deffers()
{
	TEST_ASSERT_EQUAL(false, StateValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(false, StateValidator_isValid(&validator, 2));

	TEST_ASSERT_EQUAL(false, StateValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(false, StateValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(true, StateValidator_isValid(&validator, 1));
}
