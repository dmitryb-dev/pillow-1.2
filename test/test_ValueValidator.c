#include "unity.h"
#include "service/validate/ValueValidator.h"
#include "util/utils.h"

ValueValidator validator;

void setUp() {
	validator = ValueValidator_createWithInitVal(2, 2, -10);
}
void tearDown() {}

void test_non_valid_when_not_enough_values()
{
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 2));
	TEST_ASSERT_EQUAL(true, ValueValidator_isValid(&validator, 3));
}

void test_reset_when_max_deviation_exceeded()
{
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 4)); // too big, 4 becomes origin

	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 2));
	TEST_ASSERT_EQUAL(true, ValueValidator_isValid(&validator, 3));
}

void test_correct_working_with_negative_values()
{
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 2));
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, -1));

	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(true, ValueValidator_isValid(&validator, -1));
}

void test_filter_races()
{
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 1));
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 2));
	TEST_ASSERT_EQUAL(true, ValueValidator_isValid(&validator, 1));

	TEST_ASSERT_EQUAL(true, ValueValidator_isValid(&validator, 3));
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 12)); // too big deviation
	TEST_ASSERT_EQUAL(false, ValueValidator_isValid(&validator, 11));
	TEST_ASSERT_EQUAL(true, ValueValidator_isValid(&validator, 13)); // seems value is ok
}
