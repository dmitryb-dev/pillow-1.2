#ifndef UTIL_LOGS_H
#define UTIL_LOGS_H

#include "../prefs.h"

void Logs_logText(const char* msg);
void Logs_logNumber(const int number);
void Logs_logPath(const char* group, const char* msg);

#ifdef ENABLE_LOGGING

  #define LOG(msg) Logs_logText(msg)
	#define LOG_NUMBER(value) Logs_logNumber(value)
	#define LOG_PATH(group, msg) Logs_logPath(group, msg) 

#else

	#define LOG(msg)
	#define LOG_NUMBER(value)
	#define LOG_PATH(group, msg)

#endif

#endif
