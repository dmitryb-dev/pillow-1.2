#ifndef UTIL_UTILS_H
#define UTIL_UTILS_H

#include "types.h"

#define math_abs(value) ((value) >= 0? (value) : (-value));

byte byte_abs(byte value);

float float_abs(float value);

byte check_range(byte value, byte min, byte max);

#endif
