#ifndef UTIL_BUFFER_H
#define UTIL_BUFFER_H

#include "types.h"

typedef struct {
  const byte* buffer;
  byte buffer_size;
  byte index;
} ByteBuffer;

ByteBuffer ByteBuffer_init(const byte* buffer, byte buffer_size)
{
  ByteBuffer byte_buffer = {
    .buffer = buffer,
    .buffer_size = buffer_size,
    .index = 0
  };
  return byte_buffer;
}

bool ByteBuffer_hasUnprocessedData(ByteBuffer* buffer)
{
  return buffer->index < buffer->buffer_size;
}

byte ByteBuffer_next(ByteBuffer* buffer)
{
  return buffer->buffer[buffer->index++];
}

#endif
