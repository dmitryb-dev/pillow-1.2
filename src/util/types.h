#ifndef UTIL_TYPES_H
#define UTIL_TYPES_H

typedef char bool;
#define true 1
#define false 0

typedef char byte;

typedef void (*callback)(void);

typedef char atomic_bool;
typedef char atomic_byte;

#endif
