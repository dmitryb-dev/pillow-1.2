#include "logs.h"
#include <stdio.h>
#include "../platform/logs.h"

void Logs_logText(const char* msg)
{
  platform_log(msg);
}

void Logs_logNumber(const int value)
{
  char conversion_buffer[10];
  char *buffer_ptr = (char*) &conversion_buffer;
  sprintf(buffer_ptr, "%d", value);
	
  Logs_logText(buffer_ptr);
}

void Logs_logPath(const char* group, const char* msg)
{
	char conversion_buffer[30];
  char *buffer_ptr = (char*) &conversion_buffer;
  sprintf(buffer_ptr, "%s: ", group);
	
  Logs_logText(buffer_ptr);
	Logs_logText(msg);
}
