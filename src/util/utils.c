#include "utils.h"
#include "types.h"

byte byte_abs(byte value)
{
	return math_abs(value);
}

float float_abs(float value)
{
  return math_abs(value);
}

byte check_range(byte value, byte min, byte max)
{
  if (value >= min)
  {
    if (value <= max)
    {
      return value;
    }
    return max;
  }
  return min;
}
