#ifndef UTIL_CHANNEL_H
#define UTIL_CHANNEL_H

#include "types.h"

/* ONE CONSUMER - PRODUCER THREAD SAFE CHANNEL */

typedef struct {
  atomic_byte read_count;
  atomic_byte write_count;

  atomic_byte read_index;
  atomic_byte write_index;

  byte *buffer;
  byte buffer_size;
} ByteChannel;

ByteChannel ByteChannel_create(byte* buffer, byte buffer_size);

bool ByteChannel_hasNew(ByteChannel* channel);
bool ByteChannel_hasSpace(ByteChannel* channel);

void ByteChannel_push(ByteChannel* channel, byte value);
byte ByteChannel_pop(ByteChannel* channel);


#endif
