#include "types.h"
#include "channel.h"

ByteChannel ByteChannel_create(byte* buffer, byte buffer_size)
{
  ByteChannel channel = {
    .read_count = 0,
    .write_count = 0,
    .read_index = 0,
    .write_index = 0,
    .buffer = buffer,
    .buffer_size = buffer_size
  };
  return channel;
}

bool ByteChannel_hasNew(ByteChannel* channel)
{
  return channel->write_count != channel->read_count;
}

bool ByteChannel_hasSpace(ByteChannel* channel)
{
  return channel->read_index != channel->write_index ||
    !ByteChannel_hasNew(channel);
}

void ByteChannel_push(ByteChannel* channel, byte value)
{
  channel->buffer[channel->write_index] = value;
  if (++channel->write_index >= channel->buffer_size)
  {
    channel->write_index = 0;
  }
  channel->write_count++;
}

byte ByteChannel_pop(ByteChannel* channel)
{
  byte value = channel->buffer[channel->read_index];
  if (++channel->read_index >= channel->buffer_size)
  {
    channel->read_index = 0;
  }
  channel->read_count++;
  return value;
}
