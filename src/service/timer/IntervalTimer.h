#ifndef TIMERS_INTERVAL_TIMER_H
#define TIMERS_INTERVAL_TIMER_H

#include "../../platform/interrupts.h"
#include "../../util/types.h"

void IntervalTimer_start(int tick_interval_millis);
void IntervalTimer_stop();

bool IntervalTimer_checkIsRingedAndReset();

#endif
