#include "IntervalTimer.h"
#include "../../platform/interrupts.h"
#include "../../util/types.h"
#include "../../prefs.h"
#include "../../util/logs.h"

volatile atomic_bool IntervalTimer_is_ringed;
void IntervalTimer_ring();

void IntervalTimer_start(int tick_interval_millis)
{
  LOG_PATH("IntervalTimer", "Started\n");
  interrupts_registerTimer(INTERVAL_TIMER,
    INTERVAL_TIMER_TICK_INTERVAL_MILLIS * 1000,
    IntervalTimer_ring);
}
void IntervalTimer_stop()
{
  LOG_PATH("IntervalTimer", "Stopped\n");
  interrupts_unregisterTimer(INTERVAL_TIMER);
  IntervalTimer_is_ringed = false;
}

bool IntervalTimer_checkIsRingedAndReset()
{
  if (IntervalTimer_is_ringed)
  {
    // LOG_PATH("\nIntervalTimer", "Timer ringed\n");
    IntervalTimer_is_ringed = false;
    return true;
  }
  else
  {
    // LOG_PATH("\nIntervalTimer", "Timer not ringed yet\n");
    return false;
  }
}

void IntervalTimer_ring()
{
  IntervalTimer_is_ringed = true;
}
