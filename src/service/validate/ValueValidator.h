#ifndef VALIDATE_VALUE_VALIDATOR_H
#define VALIDATE_VALUE_VALIDATOR_H

#include "../../util/types.h"

typedef struct {
	byte max_deviation;
	byte steps_number;
  byte last_value;
	byte _valid_values_counter;
} ValueValidator;

ValueValidator ValueValidator_createWithInitVal(
	byte steps, byte max_deviation, byte init_value);

ValueValidator ValueValidator_create(byte steps, byte max_deviation);

bool ValueValidator_isValid(ValueValidator* validator, byte value);

#endif
