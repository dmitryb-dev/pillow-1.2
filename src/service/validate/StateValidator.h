#ifndef VALIDATE_STATE_VALIDATOR_H
#define VALIDATE_STATE_VALIDATOR_H

#include "../../util/types.h"
#include "ValueValidator.h"

typedef struct {
	ValueValidator _decorated;
} StateValidator;

StateValidator StateValidator_createWithInitVal(byte steps, byte init_state);

StateValidator StateValidator_create(byte steps);

bool StateValidator_isValid(StateValidator* validator, byte state);

#endif
