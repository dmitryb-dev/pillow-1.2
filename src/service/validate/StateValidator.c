#include "StateValidator.h"

StateValidator StateValidator_createWithInitVal(byte steps, byte init_state)
{
  StateValidator validator = {
		._decorated = ValueValidator_createWithInitVal(steps, 0, init_state)
	};
	return validator;
}

StateValidator StateValidator_create(byte steps)
{
  return StateValidator_createWithInitVal(steps, 0);
}

bool StateValidator_isValid(StateValidator* validator, byte state)
{
  return ValueValidator_isValid(&validator->_decorated, state);
}
