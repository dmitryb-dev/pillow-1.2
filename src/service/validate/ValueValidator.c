#include "ValueValidator.h"
#include "../../util/utils.h"

ValueValidator ValueValidator_createWithInitVal(
	byte steps, byte max_deviation, byte init_value)
{
	ValueValidator validator = {
		.max_deviation = max_deviation,
		.steps_number = steps,
		.last_value = init_value,
		._valid_values_counter = 0
	};
	return validator;
}

ValueValidator ValueValidator_create(byte steps, byte max_deviation)
{
	return ValueValidator_createWithInitVal(steps, max_deviation, 0);
}

bool ValueValidator_isValid(ValueValidator* validator, byte value)
{
	if (byte_abs(validator->last_value - value) <= validator->max_deviation)
	{
		if (validator->_valid_values_counter < validator->steps_number)
			validator->_valid_values_counter++;
	}
	else
	{
		validator->last_value = value;
		validator->_valid_values_counter = 0;
	}
	return validator->_valid_values_counter >= validator->steps_number;
}
