#include "ValidationService.h"
#include "../../util/logs.h"

ValidationService ValidationService_createWithInitVal(
  bool (*is_valid)(byte), byte value)
{
  ValidationService service = {
    .last_valid_value = value,
    .is_valid = is_valid
  };
  return service;
}

ValidationService ValidationService_create(bool (*is_valid)(byte))
{
  return ValidationService_createWithInitVal(is_valid, 0);
}

byte ValidationService_processValue(ValidationService* service, byte value)
{
	if (service->is_valid(value))
	{
		LOG_PATH("ValidationService", "Valid value: "); LOG_NUMBER(value);
		service->last_valid_value = value;
	}
	else
	{
		LOG_PATH("ValidationService", "Not valid value: "); LOG_NUMBER(value);
		LOG(", using prev: "); LOG_NUMBER(service->last_valid_value);
	}
	LOG("\n");
	return service->last_valid_value;
}
