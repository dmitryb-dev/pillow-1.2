#ifndef VALIDATE_VALIDATION_SERVICE_H
#define VALIDATE_VALIDATION_SERVICE_H

#include "../../util/types.h"

typedef struct {
  byte last_valid_value;
  bool (*is_valid)(byte);
} ValidationService;

ValidationService ValidationService_createWithInitVal(
  bool (*is_valid)(byte), byte value);

ValidationService ValidationService_create(bool (*is_valid)(byte));

byte ValidationService_processValue(ValidationService* service, byte value);

#endif
