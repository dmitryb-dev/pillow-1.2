#include "PanelService.h"
#include "../util/types.h"
#include "../util/utils.h"
#include "../platform/IO.h"
#include "validate/StateValidator.h"
#include "validate/ValueValidator.h"
#include "validate/ValidationService.h"
#include "../prefs.h"
#include "../util/logs.h"

PillowMode _PanelService_mode_current;
StateValidator _PanelService_mode_validator;
bool _PanelService_mode_isValid_adapter(byte value);
ValidationService _PanelService_mode_validation_service;

ValueValidator _PanelService_volume_validator;
bool _PanelService_volume_isValid_adapter(byte value);
ValidationService _PanelService_volume_validation_service;

ValueValidator _PanelService_duration_validator;
bool _PanelService_duration_isValid_adapter(byte value);
ValidationService _PanelService_duration_validation_service;


void PanelService_init()
{
  _PanelService_mode_current = -1;

  _PanelService_mode_validator = StateValidator_create(PANEL_VALIDATION_STEPS);
  _PanelService_volume_validator = _PanelService_duration_validator =
    ValueValidator_create(PANEL_VALIDATION_STEPS, PANEL_VALIDATION_MAX_DEVIATION);

  _PanelService_mode_validation_service = ValidationService_createWithInitVal(
    _PanelService_mode_isValid_adapter, MODE_AUTO
  );
  _PanelService_volume_validation_service = ValidationService_create(
    _PanelService_volume_isValid_adapter
  );
  _PanelService_duration_validation_service = ValidationService_create(
    _PanelService_duration_isValid_adapter
  );
}

PillowMode PanelService_getMode()
{
  return _PanelService_mode_current;
}

float PanelService_getVolume()
{
  LOG_PATH("\nPanelService", "Updating volume...\n");

  float value = ValidationService_processValue(
    &_PanelService_volume_validation_service,
    IO_getValue(VOLUME_SLIDER)
  ) / 100.0f;

  LOG_PATH("PanelService", "Volume was updated: ");
  LOG_NUMBER(value * 100); LOG("%\n");

  return value;
}

float PanelService_getDuration()
{
  LOG_PATH("\nPanelService", "Updating duration...\n");

  float value = ValidationService_processValue(
    &_PanelService_duration_validation_service,
    IO_getValue(DURATION_SLIDER)
  ) / 100.0f;

  LOG_PATH("PanelService", "Duration was updated: ");
  LOG_NUMBER(value * 100); LOG("%\n");

  return value;
}


bool PanelService_updateMode()
{
	LOG_PATH("\nPanelService", "Updating mode...\n");

  PillowMode prev = _PanelService_mode_current;
  _PanelService_mode_current = ValidationService_processValue(
    &_PanelService_mode_validation_service,
    IO_getValue(WORKING_MODE_TOGGLE)
  );

  LOG_PATH("PanelService", "Mode was updated: ");
  LOG_NUMBER(_PanelService_mode_current); LOG("\n");
  return prev != _PanelService_mode_current;
}


bool _PanelService_mode_isValid_adapter(byte value)
{
  return StateValidator_isValid(&_PanelService_mode_validator, value);
}
bool _PanelService_volume_isValid_adapter(byte value)
{
  return ValueValidator_isValid(&_PanelService_volume_validator, value);
}
bool _PanelService_duration_isValid_adapter(byte value)
{
  return ValueValidator_isValid(&_PanelService_duration_validator, value);
}
