#ifndef SERVICE_UPDATE_SCHEDULER_H
#define SERVICE_UPDATE_SCHEDULER_H

#include "../util/types.h"

typedef enum {
  PANEL_UPDATE_SCHEDULE,
  HEAD_SENSOR_UPDATE_SCHEDULE,
  BATTERY_UPDATE_SCHEDULE,
  _SCHEDULES_NUMBER
} UpdateSchedule;

void UpdateSchedulerService_reset();
void UpdateSchedulerService_updateState();
bool UpdateSchedulerService_checkScheduleAndReset(UpdateSchedule schedule);

#endif
