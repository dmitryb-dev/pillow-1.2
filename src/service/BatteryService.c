#include "BatteryService.h"

#include "../util/logs.h"
#include "../platform/IO.h"
#include "validate/ValidationService.h"
#include "validate/ValueValidator.h"
#include "../prefs.h"

ValidationService BatteryService_validation_service;
ValueValidator BatteryService_value_validator;
bool BatteryService_isValid_adapter(byte value);

void BatteryService_init()
{
  BatteryService_value_validator = ValueValidator_create(
    BATTERY_VALIDATION_STEPS, BATTERY_VALIDATION_MAX_DEVIATION);
  BatteryService_validation_service =
    ValidationService_create(BatteryService_isValid_adapter);
}

BatteryLevel BatteryService_getBatteryLevel()
{
  LOG_PATH("\nBatteryService", "Updating battery level...\n");

  byte level = ValidationService_processValue(
    &BatteryService_validation_service,
    IO_getValue(BATTERY_LEVEL)
  );

  LOG_PATH("BatteryService", "Battery level updated: ");
  LOG_NUMBER(level); LOG("\n");

  return level >= BATTERY_MIN_LEVEL? BATTERY_NORMAL : BATTERY_LOW;
}

bool BatteryService_isValid_adapter(byte value)
{
  return ValueValidator_isValid(&BatteryService_value_validator, value);
}
