#ifndef SERVICE_HEAD_SENSOR_H
#define SERVICE_HEAD_SENSOR_H

#include "../util/types.h"

typedef enum {
  HEAD_IN,
  HEAD_OUT,
  SENSOR_DISABLED
} HeadSensorState;

void HeadSensorService_init(byte sensors_number, byte triggering_threshold);

void HeadSensorService_enable();
void HeadSensorService_disable();
HeadSensorState HeadSensorService_getState();

#endif
