#ifndef SERVICE_BATTERY_H
#define SERVICE_BATTERY_H

typedef enum {
  BATTERY_LOW,
  BATTERY_NORMAL
} BatteryLevel;

void BatteryService_init();

BatteryLevel BatteryService_getBatteryLevel();

#endif
