#ifndef PANEL_SERVICE_H
#define PANEL_SERVICE_H

#include "../util/types.h"

typedef enum {
	MODE_OFF,
	MODE_AUTO,
	MODE_MANUAL
} PillowMode;

void PanelService_init();

PillowMode PanelService_getMode();
float PanelService_getVolume();
float PanelService_getDuration();

bool PanelService_updateMode();

#endif
