#include "UpdateSchedulerService.h"

#include "../util/types.h"
#include "../prefs.h"

bool UpdateSchedulerService_updateTimeCome[_SCHEDULES_NUMBER];

void UpdateSchedulerService_reset()
{
  for (int i = 0; i < _SCHEDULES_NUMBER; i++)
  {
    UpdateSchedulerService_updateTimeCome[i] = 0;
  }
}

void UpdateSchedulerService_updateState()
{
  static int steps_counter = 0;
  steps_counter++;

  if (steps_counter % PANEL_UPDATE_INTERVAL_TICKS == 0)
    UpdateSchedulerService_updateTimeCome[PANEL_UPDATE_SCHEDULE] = true;

  if (steps_counter % BATTERY_UPDATE_INTERVAL_TICKS == 0)
    UpdateSchedulerService_updateTimeCome[BATTERY_UPDATE_SCHEDULE] = true;

  if (steps_counter % HEAD_SENSOR_UPDATE_INTERVAL_TICKS == 0)
    UpdateSchedulerService_updateTimeCome[HEAD_SENSOR_UPDATE_SCHEDULE] = true;
}

bool UpdateSchedulerService_checkScheduleAndReset(UpdateSchedule schedule)
{
  if (UpdateSchedulerService_updateTimeCome[schedule])
  {
    UpdateSchedulerService_updateTimeCome[schedule] = false;
    return true;
  }
  return false;
}
