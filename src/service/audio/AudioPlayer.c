#include "AudioPlayer.h"
#include "../../util/types.h"
#include "../../platform/interrupts.h"
#include "../../prefs.h"
#include "../../platform/IO.h"

void AudioPlayer_setLevel();
byte (*AudioPlayer_data_source)(void);

void AudioPlayer_start(byte (*data_source)(void))
{
  AudioPlayer_data_source = data_source;

  int update_interval = 1000 * 1000 / AUDIO_FREQ;
  interrupts_registerTimer(AUDIO_PLAY_TIMER, update_interval,
    AudioPlayer_setLevel);
}
void AudioPlayer_stop()
{
  interrupts_unregisterTimer(AUDIO_PLAY_TIMER);
}

void AudioPlayer_setLevel()
{
  IO_setAudioSignalLevel(AudioPlayer_data_source(), AudioPlayer_data_source());
}
