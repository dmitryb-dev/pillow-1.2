#ifndef SERVICE_AUDIO_PLAYER_H
#define SERVICE_AUDIO_PLAYER_H

#include "../../util/types.h"

void AudioPlayer_start(byte (*data_source)(void));
void AudioPlayer_stop();

#endif
