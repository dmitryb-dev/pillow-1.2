#include "AudioService.h"
#include "../../lifecycle/SystemState.h"
#include "../../util/logs.h"
#include "../../util/types.h"
#include "../../event/AudioChanged.h"
#include "../../prefs.h"
#include "../../util/channel.h"
#include "../../util/buffer.h"
#include "AudioPlayer.h"
#include "../../platform/IO.h"

bool AudioService_is_play = false;
float AudioService_volume;

ByteChannel AudioService_channel;
byte AudioService_buffer[AUDIO_BUFFER_SIZE];

byte AudioService_dataSource();

void AudioService_init()
{
	AudioService_channel = ByteChannel_create(AudioService_buffer,
		AUDIO_BUFFER_SIZE);
}

void AudioService_play()
{
	if (!AudioService_is_play)
	{
		AudioService_is_play = true;
		LOG_PATH("AudioService", "Play\n");

		AudioPlayer_start(AudioService_dataSource);
		AudioChanged_onPlay();
	}
}

void AudioService_stop()
{
	if (AudioService_is_play)
	{
		AudioService_is_play = false;
		LOG_PATH("AudioService", "Stop\n");

		AudioPlayer_stop();
		AudioChanged_onStop();
	}
}

void AudioService_setVolume(float volume)
{
	AudioService_volume = volume;
}

void AudioService_setDuration(float duration)
{
	// TODO
}

void AudioService_updateState()
{
	static int file_index = 0;

	#define _BUF_SIZE 8
	static byte _buffer[_BUF_SIZE];
	static ByteBuffer buffer;

	while(ByteBuffer_hasUnprocessedData(&buffer))
	{
		if (!ByteChannel_hasSpace(&AudioService_channel))
			return; // Sory, wait until audio player will consume data.

		ByteChannel_push(&AudioService_channel,
		//	AudioService_volume * ByteBuffer_next(&buffer));
			ByteBuffer_next(&buffer));
	}

	// When buffer is empty.
	byte read_size = IO_readFile(file_index, &_buffer[0]);
	if (read_size == FILE_END)
	{
		file_index = 0;
		return;
	}
	file_index += read_size;

	buffer = ByteBuffer_init(&_buffer[0], read_size);
}

byte AudioService_dataSource()
{
	if (ByteChannel_hasNew(&AudioService_channel))
	{
		return ByteChannel_pop(&AudioService_channel);
	}
	return 0;
}
