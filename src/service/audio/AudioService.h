#ifndef SERVICE_AUDIO_H
#define SERVICE_AUDIO_H

void AudioService_init();

void AudioService_play();
void AudioService_stop();

void AudioService_setVolume(float volume);
void AudioService_setDuration(float duration);

void AudioService_updateState();

#endif
