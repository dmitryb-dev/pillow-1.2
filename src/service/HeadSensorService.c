#include "HeadSensorService.h"
#include "../prefs.h"
#include "../util/types.h"
#include "../util/logs.h"
#include "service/validate/ValueValidator.h"
#include "service/validate/ValidationService.h"
#include "../platform/IO.h"

#define HEAD_SENSOR_RESERVED 40

typedef struct {
  ValueValidator value_validator;
  ValidationService validation_service;
} HeadSensorValidation;
HeadSensorValidation HeadSensorService_validators[HEAD_SENSOR_RESERVED];

bool HeadSensorService_isValidAdapter(byte value);
HeadSensorState HeadSensorService_resolveState(byte* data);

int HeadSensorService_sensor_index;
bool HeadSensorService_isEnabled;
byte HeadSensorService_triggering_threshold;
byte HeadSensorService_sensors_number;

void HeadSensorService_init(byte sensors_number, byte triggering_threshold)
{
  HeadSensorService_triggering_threshold = triggering_threshold;
  HeadSensorService_sensors_number = sensors_number;
  for (int i = 0; i < HeadSensorService_sensors_number; i++)
  {
    HeadSensorValidation validator = {
      .value_validator = ValueValidator_create(HEAD_SENSOR_VALIDATION_STEPS,
        HEAD_SENSOR_VALIDATION_MAX_DEVIATION),
      .validation_service = ValidationService_create(
        HeadSensorService_isValidAdapter)
    };
    HeadSensorService_validators[i] = validator;
  }
}

void HeadSensorService_enable()
{
  HeadSensorService_isEnabled = true;
  LOG_PATH("\nHeadSensorService", "Head sensor enabled.\n");
}

void HeadSensorService_disable()
{
  HeadSensorService_isEnabled = false;
  LOG_PATH("\nHeadSensorService", "Head sensor disabled.\n");
}

byte* HeadSensorService_validation_process(byte* data);

HeadSensorState HeadSensorService_getState()
{
  if (!HeadSensorService_isEnabled)
  {
    LOG_PATH("\nHeadSensorService", "Head sensor is disabled, skip.\n");
    return SENSOR_DISABLED;
  }

  LOG_PATH("\nHeadSensorService", "Updating data...\n");

  byte* data = HeadSensorService_validation_process(IO_getHeadSensorData());

  LOG_PATH("HeadSensorService", "Data retrieved and validated: ");
  for (int i = 0; i < HeadSensorService_sensors_number; i++)
  {
    LOG_NUMBER(data[i]); LOG(" ");
  }
  LOG("\n");
	
  return HeadSensorService_resolveState(data);
}

int HeadSensorService_sensor_index;
bool HeadSensorService_isValidAdapter(byte value)
{
  ValueValidator *validator = &HeadSensorService_validators
    [HeadSensorService_sensor_index].value_validator;
  return ValueValidator_isValid(validator, value);
}

byte* HeadSensorService_validation_process(byte* data)
{
  static byte buffer[HEAD_SENSOR_RESERVED];

  for (int i = 0; i < HeadSensorService_sensors_number; i++)
  {
    ValidationService *validation_service = &HeadSensorService_validators[i]
      .validation_service;
    HeadSensorService_sensor_index = i;
    buffer[i] = ValidationService_processValue(validation_service, data[i]);
  }
  return buffer;
}

HeadSensorState HeadSensorService_resolveState(byte* data)
{
  static const byte sensor_value_to_importance[] =
    {  0,  1,  4,  9, 16,
      25, 36, 49, 64, 81 };

  int avg = 0;
  for (int i = 0; i < HeadSensorService_sensors_number; i++)
  {
    avg += sensor_value_to_importance[data[i]];
  }
	
  avg /= HeadSensorService_sensors_number;

  LOG_PATH("HeadSensorService", "Averange sensor data: ");
  LOG_NUMBER(avg); LOG("; Triggering threshold: ");
  LOG_NUMBER(sensor_value_to_importance[HeadSensorService_triggering_threshold]);
  LOG("\n");

  if (sensor_value_to_importance[HeadSensorService_triggering_threshold]
    <= avg)
  {
    LOG_PATH("HeadSensorService", "Head is on pillow\n");
    return HEAD_IN;
  }
  LOG_PATH("HeadSensorService", "Head is out of pillow\n");
  return HEAD_OUT;
}
