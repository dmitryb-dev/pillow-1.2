#ifndef PLATFORM_IO_H
#define PLATFORM_IO_H

#include "../util/types.h"

typedef enum {
	WORKING_MODE_TOGGLE, // 0, 1, 2
	VOLUME_SLIDER, 			 // 0-100
	DURATION_SLIDER,     // 0-100
	BATTERY_LEVEL        // 0-10
} InputPort;

byte IO_getValue(InputPort port);

// params left & right: (-100)-100
void IO_setAudioSignalLevel(byte left, byte right);


#define FILE_END -1
// returns number of read bytes. Recomended size of buffer - 8 bytes.
byte IO_readFile(int read_pos, byte* destination);

// values of sensors. 0-9 for each sensor
byte* IO_getHeadSensorData();

#endif
