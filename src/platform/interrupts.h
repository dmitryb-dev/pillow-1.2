#ifndef PLATFORM_INTERRUPTS_H
#define PLATFORM_INTERRUPTS_H

typedef enum {
  INTERVAL_TIMER,
  AUDIO_PLAY_TIMER
} Timer;

void interrupts_registerTimer(Timer timer, int interval_nanos,
  void (*callback)(void));
void interrupts_unregisterTimer(Timer timer);

void interrupts_sleep(int millis);
void interrupts_deep_sleep();

#endif
