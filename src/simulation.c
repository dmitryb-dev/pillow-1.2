#define ENABLE_LOGGING

#include "platform/IO.h"
#include "platform/interrupts.h"
#include "util/logs.h"
#include "util/utils.h"
#include "../test/support/unix_logs.h"
#include "lifecycle/lifecycle.h"
#include "service/PanelService.h"
#include "service/validate/ValueValidator.h"
#include "service/validate/StateValidator.h"
#include "service/validate/ValidationService.h"

#include <pthread.h>

int simulation_run;
byte mode = -1, volume, duration, head_sensor, battery;
void* pillow_input_simulation(void* arg)
{
	LOG("\n--- INPUT SIMULATION STARTED ---\n\n");
	simulation_run++;
	while(simulation_run)
	{
		switch(getchar())
		{
			case '1': mode = 0; break;
			case '2': mode = 1; break;
			case '3': mode = 2; break;

			case 'q': volume = 0; break;
			case 'w': volume = 25; break;
			case 'e': volume = 50; break;
			case 'r': volume = 75; break;
			case 't': volume = 100; break;

			case 'a': duration = 0; break;
			case 's': duration = 25; break;
			case 'd': duration = 50; break;
			case 'f': duration = 75; break;
			case 'g': duration = 100; break;

			case 'z': head_sensor = 0; break;
			case 'x': head_sensor = 2; break;
			case 'c': head_sensor = 5; break;
			case 'v': head_sensor = 7; break;
			case 'b': head_sensor = 10; break;

			case 'i': battery = 0; break;
			case 'o': battery = 2; break;
			case 'p': battery = 5; break;
			case '[': battery = 7; break;
			case ']': battery = 10; break;
		}
	}
	LOG("\n--- INPUT SIMULATION STOPPED ---\n\n");
	return 0;
}

int main_timer_interval, audio_timer_interval;
void (*main_timer_callback)(void);
void (*audio_timer_callback)(void);
void* main_timer_simulation(void* arg)
{
	LOG("\n--- INTERVAL TIMER SIMULATION STARTED ---\n\n");
	simulation_run++;
	while (simulation_run > 1)
		if (main_timer_callback)
		{
			main_timer_callback();
			usleep(main_timer_interval);
		}
	LOG("\n--- INTERVAL TIMER SIMULATION STOPPED ---\n\n");
	return 0;
}
void* audio_timer_simulation(void* arg)
{
	LOG("\n--- AUDIO TIMER SIMULATION STARTED ---\n\n");
	simulation_run++;
	while (simulation_run > 2)
		if (audio_timer_callback)
		{
			audio_timer_callback();
			usleep(audio_timer_interval);
		}
	LOG("\n--- AUDIO TIMER SIMULATION STOPPED ---\n\n");
	return 0;
}

byte IO_getValue(InputPort port)
{
	switch (port)
	{
		case WORKING_MODE_TOGGLE: return mode;
		case VOLUME_SLIDER: return volume;
		case DURATION_SLIDER: return duration;
		case BATTERY_LEVEL: return battery;
	}
	return 0;
}

void interrupts_registerTimer(Timer timer, int interval_nanos,
  void (*callback)(void))
{
	switch (timer)
	{
		case INTERVAL_TIMER:
			main_timer_interval = interval_nanos;
			main_timer_callback = callback;
			break;
		case AUDIO_PLAY_TIMER:
			audio_timer_interval = interval_nanos;
			audio_timer_callback = callback;
			break;
	}
}
void interrupts_unregisterTimer(Timer timer)
{
	switch (timer)
	{
		case INTERVAL_TIMER:
			main_timer_callback = 0;
			break;
		case AUDIO_PLAY_TIMER:
			audio_timer_callback = 0;
			break;
	}
}

void interrupts_sleep(int millis)
{
	usleep(1000 * millis);
}
void interrupts_deep_sleep()
{
	usleep(1000 * 1000);
}

byte IO_readFile(int read_pos, byte* destination)
{
	*destination = read_pos;
	return sizeof(char);
}
void IO_setAudioSignalLevel(byte left, byte right)
{
	LOG(">>> Left: "); LOG_NUMBER(left); LOG(", Right: "); LOG_NUMBER(right);
	LOG("\n");
}

byte* IO_getHeadSensorData()
{
	return &head_sensor;
}

int main()
{
	pthread_t input_thread_id, main_timer_thread_id, audio_timer_thread_id;
	pthread_create(&input_thread_id, 0, &pillow_input_simulation, 0);
	while(simulation_run < 1) {}
	pthread_create(&main_timer_thread_id, 0, &main_timer_simulation, 0);
	while(simulation_run < 2) {}
	pthread_create(&audio_timer_thread_id, 0, &audio_timer_simulation, 0);
	while(simulation_run < 3) {}

	Pillow_run();

	simulation_run--;
	pthread_join(audio_timer_thread_id, 0);
	simulation_run--;
	pthread_join(main_timer_thread_id, 0);
	simulation_run--;
	pthread_join(input_thread_id, 0);
	return 0;
}
