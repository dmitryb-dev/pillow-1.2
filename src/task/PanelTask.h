#ifndef PANEL_TASK_H
#define PANEL_TASK_H

#include "../util/types.h"
#include "../service/PanelService.h"
#include "../service/audio/AudioService.h"
#include "../event/ModeChanged.h"

void PanelTask_init()
{
	PanelService_init();
}

bool PanelTask_hasLostChanges;

void PanelTask_updateIfOFFMode()
{
	if (PanelService_updateMode())
	{
		if (PanelService_getMode() == MODE_OFF)
		{
			PanelTask_hasLostChanges = false;
			ModeChanged_riseEvent(MODE_OFF);
		}
		else
		{
			PanelTask_hasLostChanges = true;
		}
	}
}

void PanelTask_forceUpdateMode()
{
	PanelTask_hasLostChanges = false;
	ModeChanged_riseEvent(PanelService_getMode());
}

void PanelTask_updateState()
{
	if (PanelService_updateMode() || PanelTask_hasLostChanges)
	{
		PanelTask_hasLostChanges = false;
		ModeChanged_riseEvent(PanelService_getMode());
	}

	AudioService_setVolume(PanelService_getVolume());
	AudioService_setDuration(PanelService_getDuration());
}

#endif
