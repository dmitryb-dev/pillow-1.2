#ifndef TASK_TIMER_H
#define TASK_TIMER_H

#include "../service/timer/IntervalTimer.h"
#include "../event/TimerTick.h"

void TimerTask_updateState()
{
  if (IntervalTimer_checkIsRingedAndReset())
  {
    TimerTick_onTick();
  }
}

#endif
