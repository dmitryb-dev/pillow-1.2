#ifndef AUDIO_TASK_H
#define AUDIO_TASK_H

#include "../service/audio/AudioService.h"

void AudioTask_init()
{
  AudioService_init();
}

void AudioTask_updateState()
{
  AudioService_updateState();
}

#endif
