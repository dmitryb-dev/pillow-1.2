#ifndef HEAD_SENSOR_TASK_H
#define HEAD_SENSOR_TASK_H

#include "../service/HeadSensorService.h"
#include "../event/HeadSensorStateChanged.h"

void HeadSensorTask_init()
{
  HeadSensorService_init(HEAD_SENSORS_NUMBER,
    HEAD_SENSOR_TRIGGERING_THRESHOLD);
}

void HeadSensorTask_updateState()
{
  static HeadSensorState prev_state = -1;

  HeadSensorState state = HeadSensorService_getState();
  if (prev_state != HEAD_IN && state == HEAD_IN)
  {
    HeadSensorStateChanged_onHeadIn();
  }
  else if(prev_state == HEAD_IN && state != HEAD_IN)
  {
    HeadSensorStateChanged_onHeadOut();
  }
  prev_state = state;
}

#endif
