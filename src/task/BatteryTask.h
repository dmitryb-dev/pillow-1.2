#ifndef BATTERY_TASK_H
#define BATTERY_TASK_H

#include "../util/types.h"
#include "../event/BatteryLevelChanged.h"
#include "../service/BatteryService.h"

void BatteryTask_init()
{
	BatteryService_init();
}

void BatteryTask_updateState(SystemState state)
{
	if (state != STATE_LOW_ENERGY)
	{
		if (BatteryService_getBatteryLevel() == BATTERY_LOW)
		{
			LOG_PATH("BatteryTask", "Battery level is low\n");
			BatteryLevelChanged_onLowLevel();
		}
	}
	else
	{
		if (BatteryService_getBatteryLevel() == BATTERY_NORMAL)
		{
			LOG_PATH("BatteryTask", "Battery level is normal\n");
			BatteryLevelChanged_onNormalLevel();
		}
	}
}

#endif
