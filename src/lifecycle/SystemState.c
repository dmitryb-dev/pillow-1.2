#include "SystemState.h"

#include "../event/SystemStateChanged.h"

SystemState SystemState_current = -1;
void SystemState_applyState();

void SystemState_setState(SystemState state)
{
	if (SystemState_current == state)
		return;
	else
		SystemState_current = state;

	StateChanged_onStateChanged(state);
	SystemState_applyState(state);
}

SystemState SystemState_getState()
{
  return SystemState_current;
}

void SystemState_applyState(SystemState state)
{
  LOG_PATH("SystemState", "New system state: ");
  switch(state)
  {
    case STATE_NORMAL:
      LOG("NORMAL");
      SystemState_run = SystemState_normal;
      break;

    case STATE_LOW_ENERGY:
      LOG("LOW_ENERGY");
      SystemState_run = SystemState_lowEnergy;
      break;

    case STATE_AUDIO_PLAY:
      LOG("AUDIO_PLAY");
      SystemState_run = SystemState_audioPlay;
      break;
  }
  LOG("\n");
}
