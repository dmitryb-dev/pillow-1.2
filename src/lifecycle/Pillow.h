#ifndef LIFECYCLE_PILLOW_H
#define LIFECYCLE_PILLOW_H

#include "SystemState.h"
#include "../util/types.h"
#include "../util/logs.h"

void Pillow_init();

bool Pillow_is_alive;

void Pillow_stop()
{
  Pillow_is_alive = false;
}

void Pillow_run()
{
  LOG_PATH("Pillow", "Pillow started\n");
  Pillow_init();
  Pillow_is_alive = true;
  while(Pillow_is_alive)
  {
    SystemState_run();
  }
  LOG_PATH("\nPillow", "Pillow finished\n");
}

#endif
