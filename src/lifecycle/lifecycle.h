#ifndef LIFECYCLE_LIFECYCLE_H
#define LIFECYCLE_LIFECYCLE_H

#include "SystemState.h"
#include "Pillow.h"
#include "../service/UpdateSchedulerService.h"
#include "../task/BatteryTask.h"
#include "../task/AudioTask.h"
#include "../task/PanelTask.h"
#include "../task/HeadSensorTask.h"
#include "../task/TimerTask.h"
#include "../util/logs.h"
#include "../platform/interrupts.h"
#include "../prefs.h"

void Pillow_init()
{
	PanelTask_init();
	BatteryTask_init();
	HeadSensorTask_init();
	AudioTask_init();

	SystemState_setState(STATE_NORMAL);
}

void SystemState_normal()
{
	interrupts_sleep(STATE_NORMAL_UPDATE_INTERVAL_MILLIS);

	LOG("\n --- System step: normal ---\n");

  HeadSensorTask_updateState();
  BatteryTask_updateState(STATE_NORMAL);
	PanelTask_updateState();
}

void SystemState_lowEnergy()
{
	interrupts_deep_sleep();

	LOG("\n --- System step: low energy ---\n");

  BatteryTask_updateState(STATE_LOW_ENERGY);
	PanelTask_updateIfOFFMode();
}

void _BatteryTask_updateState_audioPlayAdapter();
void SystemState_audioPlay()
{
  // LOG("\n --- System step: audio play ---\n");

	AudioTask_updateState();

	TimerTask_updateState();

  // interrupts_sleep(STATE_NORMAL_UPDATE_INTERVAL_MILLIS / 5); // Just for testing
}

#endif
