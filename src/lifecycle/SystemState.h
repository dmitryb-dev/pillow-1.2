#ifndef LIFECYCLE_STATE_H
#define LIFECYCLE_STATE_H

#include "../util/logs.h"

typedef enum {
  STATE_NORMAL,
  STATE_LOW_ENERGY,
  STATE_AUDIO_PLAY
} SystemState;

void (*SystemState_run)(void);

void SystemState_normal();
void SystemState_lowEnergy();
void SystemState_audioPlay();

void SystemState_setState(SystemState state);
SystemState SystemState_getState();

#endif
