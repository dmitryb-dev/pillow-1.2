#ifndef EVENT_AUDIO_CHANGED_H
#define EVENT_AUDIO_CHANGED_H

#include "../lifecycle/SystemState.h"

void AudioChanged_onPlay()
{
	SystemState_setState(STATE_AUDIO_PLAY);	
}

void AudioChanged_onStop()
{
	SystemState_setState(STATE_NORMAL);
}

#endif
