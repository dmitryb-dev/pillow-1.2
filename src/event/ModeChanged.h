#ifndef EVENT_MODE_CHANGED_H
#define EVENT_MODE_CHANGED_H

#include "../service/PanelService.h"
#include "../service/audio/AudioService.h"
#include "../service/HeadSensorService.h"
#include "../lifecycle/Pillow.h"
#include "../task/HeadSensorTask.h"

void ModeChanged_onOffStateChosen()
{
	HeadSensorService_disable();
	AudioService_stop();
	Pillow_stop();
}

void ModeChanged_onManualStateChosen()
{
	HeadSensorService_disable();
	HeadSensorTask_updateState();
	AudioService_play();
}

void ModeChanged_onAutoStateChosen()
{
	AudioService_stop();
	HeadSensorService_enable();
}

void ModeChanged_riseEvent(PillowMode mode)
{
	LOG_PATH("ModeChanged", "Mode was changed: ");
	void (*listener)(void) = 0;
	switch (mode)
	{
		case MODE_OFF:
			LOG("OFF");
			listener = ModeChanged_onOffStateChosen;
			break;

		case MODE_MANUAL:
			LOG("MANUAL");
			listener = ModeChanged_onManualStateChosen;
			break;

		case MODE_AUTO:
			LOG("AUTO");
			listener = ModeChanged_onAutoStateChosen;
			break;
	}
	LOG("\n");

	if (listener) listener();
}

#endif
