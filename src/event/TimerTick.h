#ifndef EVENT_TIMER_TICK_H
#define EVENT_TIMER_TICK_H

#include "../service/UpdateSchedulerService.h"
#include "../lifecycle/SystemState.h"

void TimerTick_onTick()
{
  UpdateSchedulerService_updateState();

  if (SystemState_getState() == STATE_AUDIO_PLAY)
  {
    if (UpdateSchedulerService_checkScheduleAndReset(
      HEAD_SENSOR_UPDATE_SCHEDULE))
  	{
      HeadSensorTask_updateState();
  	}
    if (UpdateSchedulerService_checkScheduleAndReset(BATTERY_UPDATE_SCHEDULE))
  	{
  		BatteryTask_updateState(STATE_AUDIO_PLAY);
  	}
  	if (UpdateSchedulerService_checkScheduleAndReset(PANEL_UPDATE_SCHEDULE))
  	{
  		PanelTask_updateState();
  	}
  }
}

#endif
