#ifndef EVENT_HEAD_SENSOR_STATE_CHANGED_H
#define EVENT_HEAD_SENSOR_STATE_CHANGED_H

void HeadSensorStateChanged_onHeadIn()
{
  AudioService_play();
}

void HeadSensorStateChanged_onHeadOut()
{
  AudioService_stop();
}

#endif
