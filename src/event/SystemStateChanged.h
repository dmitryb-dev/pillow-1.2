#ifndef EVENT_SYSTEM_STATE
#define EVENT_SYSTEM_STATE

#include "../lifecycle/SystemState.h"
#include "../service/timer/IntervalTimer.h"
#include "../service/UpdateSchedulerService.h"
#include "../prefs.h"

void SystemState_riseOnAudioPlayStateChangeEvent();

void StateChanged_onStateChanged(SystemState state)
{
	SystemState_riseOnAudioPlayStateChangeEvent(state);
}

void StateChanged_onAudioPlayStateIn()
{
  IntervalTimer_start(INTERVAL_TIMER_TICK_INTERVAL_MILLIS);
}

void StateChanged_onAudioPlayStateOut()
{
  IntervalTimer_stop();
  UpdateSchedulerService_reset();
}

void SystemState_riseOnAudioPlayStateChangeEvent(SystemState state)
{
	static SystemState prev_state = -1;

	if (prev_state == STATE_AUDIO_PLAY && state != STATE_AUDIO_PLAY)
  {
    StateChanged_onAudioPlayStateOut();
  }
  else if (prev_state != STATE_AUDIO_PLAY && state == STATE_AUDIO_PLAY)
  {
    StateChanged_onAudioPlayStateIn();
  }
  prev_state = state;
}


#endif
