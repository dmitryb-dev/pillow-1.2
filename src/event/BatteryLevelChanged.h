#ifndef EVENT_BATTERY_LEVEL
#define EVENT_BATTERY_LEVEL

#include "../lifecycle/SystemState.h"
#include "../task/PanelTask.h"
#include "../service/audio/AudioService.h"
#include "../util/logs.h"

void BatteryLevelChanged_onLowLevel()
{
  AudioService_stop();
  SystemState_setState(STATE_LOW_ENERGY);
}

void BatteryLevelChanged_onNormalLevel()
{
  SystemState_setState(STATE_NORMAL);
  PanelTask_forceUpdateMode();
}

#endif
